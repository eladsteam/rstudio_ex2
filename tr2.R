titanic <- read.csv('train.csv')

# open window for dropData
View(titanic)

#removing columns
dropData <- titanic[-c(1,4,9,11)]

# open window for dropData
View(dropData)

# (Survived column) how to take column with numeric, and instead put names, (1 = true, 2 = false)
SurviveFactor <- dropData$Survived #take column from table dropData
SurviveFactor <- factor(SurviveFactor) #count the range od the column, here you between which numbers your column
SurviveFactor <- factor(SurviveFactor, labels = c("dead","survived")) #change the numbers (0,1) to names (dead,survived)
dropData$Survived <- SurviveFactor # here we are changing the column
dropData
# change column survived has been done

#(pclass column) change numeric to names,
PclassFactor <- dropData$Pclass #execute the column pclass from table
PclassFactor <- factor(PclassFactor) #check numeric range
PclassFactor <- factor(PclassFactor, labels = c("one","two","three")) #change numeric by names
PclassFactor
dropData$Pclass <- PclassFactor
# change column PclassFactor has been done

#(sibsp column) change numeric to names,
sibFactor <- dropData$SibSp
sibFactor <- factor(sibFactor)
sibFactor <-factor(sibFactor,labels = c("zero","one","two","three","four","five","eight"))
dropData$SibSp <- sibFactor
View(dropData)
# change column sibsp has been done

#(parch column) change numeric to names,
parchFactor <- dropData$Parch
parchFactor <- factor(parchFactor)
parchFactor <- factor(parchFactor, labels = c("zero","one","two","three","four","five","six"))
dropData$Parch <- parchFactor
View(dropData)
# change column parch has been done

#function take the column age from table and per the range create x with the data

convertAge <-  function(x){
  
  x <- ifelse(x<15,"young",
              ifelse(x<30,"mature","old"))
    x<- ifelse(is.na(x),"mature",x)
}

removeAge <- sapply(dropData$Age, convertAge) #create column per any row, take rows from Age in dropData table, send her to function and reload it on the removeAge


#back here
i1 <- 0
i2 <- 0
i3 <- 0

sumAge <-  function(x){
  
  x <- ifelse(x<15,i1<- +1,
                    ifelse(x<30,i2+1,i3+1))
  
}

countAge <- sapply(dropData$age, sumAge)

#back here

dropData$Age <- removeAge #insrt the old young mature into Age column 

#Package for algoritem naiveBayes
install.packages('e1071')
library(e1071)

conv1 <- function(x){
  
  x <- ifelse(x == "dead",0,1)
}

surviveNum <- sapply(dropData$Survived, conv1)


#Run the algoritem 
model <- naiveBayes(dropData[,-1], dropData$Survived)
model

#install SMDTOOLD
install.packages('SDMTools')
library(SDMTools)

#we need to revert 



# now we should have the test file, use it and compare it to the train file, acully we gonna get compare between the files.
titanicTest <- read.csv('test.csv')

# open window for dropData
View(titanicTest)

#removing columns
titanicTest <- titanicTest[-c(1,3,8,9,10)]
dropData <- dropData[-c(7)]
View(titanicTest)




#(pclass column) change numeric to names,
PclassFactor <- titanicTest$Pclass #execute the column pclass from table
PclassFactor <- factor(PclassFactor) #check numeric range
PclassFactor <- factor(PclassFactor, labels = c("one","two","three")) #change numeric by names
PclassFactor
titanicTest$Pclass <- PclassFactor
# change column PclassFactor has been done

#(sibsp column) change numeric to names,
sibFactor <- titanicTest$SibSp
sibFactor <- factor(sibFactor)
sibFactor <-factor(sibFactor,labels = c("zero","one","two","three","four","five","eight"))
titanicTest$SibSp <- sibFactor
View(titanicTest)
# change column sibsp has been done

#(parch column) change numeric to names,
parchFactor1 <- titanicTest$Parch
parchFactor1 <- factor(parchFactor1)
parchFactor <- factor(parchFactor, labels = c("zero","one","two","three","four","five","six","nine"))
titanicTest$Parch <- parchFactor
View(titanicTest)
# change column parch has been done

convertAge1 <-  function(x){
  
  x <- ifelse(x<15,"young",
              ifelse(x<30,"mature","old"))
  x<- ifelse(is.na(x),"mature",x)
}

removeAge1 <- sapply(titanicTest$Age, convertAge1) #create column per any row, take rows from Age

titanicTest$Age <- removeAge1

prediction1 <- predict(model, titanicTest)
prediction




